library(dplyr); library(ggnetwork); library(igraph); library(intergraph)
library(reshape2); library(tm)

MatrixToDF <- function (data = data) {
  # Remove upper triangle in matrix and melt the data.
  #
  # Args:
  #   data: Input data frame.
  # Returns:
  #

  data[upper.tri(data, diag = T)] <- NA #replace upper triangle with NA
  data %>%
    reshape2::melt() %>%
    filter(!is.na(value), value > 0)
}


Cooccurrence <- function (data = data) {
  # Calculate the words co-occurence frequency.
  #
  # Args:
  #   data: Input data frame.
  # Returns:
  #

  data %>%
    # calculate the co occurence
    crossprod() %>%
    # melt the matrix to data frame
    MatrixToDF() %>%
    # calculate in how many entries there is no such Cooccurrence
    mutate(valueNo = nrow(data) - value)
}


FrequencyMatrix <- function (data = data, target = T, iKey = iKey,
                             selectWords = selectWords) {
  # Create a matrix with words as colnames and 0 or 1 if word is present within
  # the claim.
  #
  # Args:
  #   data: Input data frame.
  #   target: TRUE or FALSE, if we shall calculate it for targe claim or all
  #   others
  #   iKey: What is the key claim.
  #   selectWords: For which words we shall calculate it. If FALSE than all
  #   words are included
  # Returns:
  #

  # Check if we calculate it for target or all others
  if (target) {
    data <- data %>% filter(Claim %in% iKey)
  } else {
    data <- data %>% filter(!Claim %in% iKey)
  }
  if(is.character(selectWords)) {
    data <- data[, c("Claim", selectWords)]
  }
  # select only words that have nodes
  data %>%
    # remove the column with Claim
    select(-Claim) %>%
    # transform data to matrix
    as.matrix() %>%
    pmin(., 1)
}


FrequencyDF <- function (data = data) {
  # Calculate the frequency of the words.
  #
  # Args:
  #   data: A matrix as an output from the 'FrequencyMatrix' function.
  # Returns:
  #

  data %>%
    as.data.frame.matrix() %>%
    # calculate frequency (occurence (maximum one per document)) of each word
    summarise_each(funs(sum)) %>%
    reshape2::melt() %>%
    dplyr::rename(word = variable, n = value)
}


ChisqColour <- function (x1, x2, y1, y2) {
  # Calculate the chi square by entering four values.
  #
  # Args:
  #   x1, x2, y1, y2: Values for calculating chisquare.
  # Returns:
  #

  if(!is.na(y1)) {
    chi <- chisq.test(as.matrix(cbind(c(x1, x2), c(y1, y2))))[["p.value"]]
    # assign color based if the difference is signifficant or not
    if(chi <= 0.05) {
      # difference in proportion of Cooccurrence
      percentDifference <- x1 / (x1 + x2) - y1 / (y1 + y2)
      ifelse(percentDifference < 0, "below average", "above average")
    } else {
      "non significant"
    }
  } else {
    "non significant"
  }
}


WordFreqIgraph <- function(data = data.df) {
  # Calculate the word frequency and see if we can move further.
  # Args:
  #   data: A data frame
  # Returns:
  #

  # Create a corpus matrix with words frequencies (use only words that occured
  # more than minimum frequency)
  data.corpus <- VCorpus(DataframeSource(data),
                         readerControl = list(reader = readTabular(mapping = list(content = "Text",
                                                                                  id = "Claim"))))

  data.dtm <- DocumentTermMatrix(data.corpus,
                                 control = list(bounds = list(global = c(2, Inf))))
  wordFreq <- as.data.frame(as.matrix(data.dtm))
  wordFreq$Claim <- row.names(wordFreq)
  return(wordFreq)
}


IgraphData <- function(data = data.df,
                       iKey = "Soft_SkinNow_EN",
                       seed_word = c("new", "soft", "nivea"),
                       n_word = 10) {
  # Calculate the words frequency for target and others and calculate the
  # significance in difference, also create a data frame for the edges.
  #
  # Args:
  #   data:
  #   iKey:
  #   seed_word:
  #   n_word:
  # Returns:
  #   A list containing data frames for the nodes and for the edges.

  options(warn = -1)
  # Number of target and competitor answers (rows in df).
  nTarget <- sum(rownames(data) %in% iKey)
  nCompetitor <- sum(!rownames(data) %in% iKey)

  # Select only the seed words or the once that are above certain threshold.
  selectWord <- data %>%
    FrequencyMatrix(., target = T, iKey = iKey, selectWords = F) %>%
    FrequencyDF() %>%
    filter(word %in% seed_word | n >= nTarget * n_word / 100) %>%
    arrange(word) %>% .$word %>% as.character()

  # We do this to rearrange the words in correct order: first seed, then in
  # alphabetic order, also we check if by chance one of the seed words is not
  # in the range.
  selectWord <- unique(c(seed_word[seed_word %in% selectWord], selectWord))

  if(length(selectWord) != 0) { # if there is at least one word
    # Create nodes data frame.
    nodeTarget <- data %>%
      FrequencyMatrix(., target = T, iKey = iKey, selectWords = selectWord) %>%
      FrequencyDF() %>%
      mutate(nNo = nTarget - n)

    nodeCompetitor <- data %>%
      FrequencyMatrix(., target = F, iKey = iKey, selectWords = selectWord) %>%
      FrequencyDF() %>%
      mutate(nNo = nCompetitor - n)

    node.df <- merge(nodeTarget,
                     nodeCompetitor,
                     by = c("word"),
                     suffixes = c("", ".all"),
                     all.x = T) %>%
      rowwise() %>%
      mutate(nodeColor = ChisqColour(n, nNo, n.all, nNo.all),
             nodeSize = n * 100 / (n + nNo)) %>%
      select(word, nodeColor, nodeSize)


    # Graph the data and create edge data frame.
    if(nrow(node.df) > 1) {
      edgeTarget <- data %>%
        FrequencyMatrix(., target = T, iKey = iKey,
                        selectWords = selectWord) %>%
        Cooccurrence()

      edgeCompetitor <- data %>%
        FrequencyMatrix(., target = F, iKey = iKey,
                        selectWords = selectWord) %>%
        Cooccurrence()

      igraphData <- merge(edgeTarget,
                          edgeCompetitor,
                          by = c("X1", "X2"),
                          suffixes = c("", ".all"),
                          all.x = T) %>%
        rowwise() %>%
        mutate(edgeColor = ChisqColour(value, valueNo, value.all, valueNo.all),
               edgeWidth = value * 100 / (value + valueNo)) %>%
        select(X1, X2, edgeWidth, edgeColor) %>%
        graph.data.frame(., directed = F) %>%
        ggnetwork(., layout = "circle") %>%
        as.data.frame.matrix(stringsAsFactors = F) %>%
        dplyr::rename(word = vertex.names)

      # Finally we separate everything to two data frames: for node and for
      # edge
      node.df <- merge(node.df,
                       igraphData %>% filter(is.na(edgeColor)) %>%
                         select(word, x, y),
                       by = "word",
                       all.x = T)

      edge.df <- igraphData %>%
        filter(!is.na(edgeColor)) %>%
        select(word, x, y, xend, yend, edgeColor, edgeWidth)

      data <- list(nodes = node.df, edges = edge.df)
      return(data)
    } else {
      stop("Not enough nodes.")
    }
  } else {
    stop("There are no target words.")
  }
}