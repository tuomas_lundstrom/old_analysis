library(dplyr)

SelectGroup <- function(data = data.df, group = "group",
                        select_group = "Nivea") {
  # Includes only the data from the specified group to be plotted.
  #
  # Args:
  #   data: Data frame as an output from the 'RemoveNALikert' function.
  #   group: Column for the grouping variable.
  #   select_group: If we split data by group, then which group to use, e.g.
  #   "Old", "Young".
  # Returns:
  #   A data frame containing only the values from the group that was selected.

  if (!any(select_group %in% "all")) {
    l <- paste0(group, " %in% '", select_group, "'")
    data <- data %>% filter_(l)
  }
  return(data)
}
