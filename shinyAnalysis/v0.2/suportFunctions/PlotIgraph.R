library(ggplot2)

PlotIgraph <- function(data = data.df) {
  # Plot the synapse map.
  #
  # Args:
  #   data:
  # Returns:
  #   A beautiful synapse map.
  
  p <- ggplot(data = data[["nodes"]], aes(x = x, y = y)) +
    theme_blank() +
    theme(legend.position = "none") +
    geom_edges(data = data[["edges"]],
               aes(xend = xend, yend = yend, size = edgeWidth / 30,
                   color = edgeColor),
               alpha = 0.5) +
    geom_nodes(aes(size = nodeSize * 20, color = nodeColor),
               alpha = 0.8) +
    geom_nodetext(aes(label = word, size = nodeSize * 3),
                  fontface = "bold") +
    scale_size_continuous(range = c(1, 40)) +
    scale_color_manual(values = c("non significant" = "grey80",
                                  "below average" = "#da2c3a",
                                  "above average" = "#78c2ef"))
  print(p)
}