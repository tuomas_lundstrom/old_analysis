library(dplyr)

SummariseRecognition <- function(data = data.df) {
  # Summarise the results of the recognition task.
  #
  # Args:
  #   data: A data frame as an output from the 'SelectGroup' function.
  # Returns:
  #   A data frame containing either the proportions of correctly recognised
  #   target or a total amount of correct answers.

  if (is.logical(data$y_val)) {
    data <- data %>%
      group_by(x_var) %>%
      dplyr::summarise(y_val = sum(y_val, na.rm = T) * 100 / length(y_val))
  } else {
    data <- data %>%
      group_by(x_var) %>%
      dplyr::summarise(y_val = sum(y_val, na.rm = T))
  }
}
