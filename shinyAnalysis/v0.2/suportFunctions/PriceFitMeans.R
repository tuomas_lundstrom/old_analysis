library(dplyr)

PriceFitMeans <- function(data = data.df) {
  # Calculate the total score, mean, and extract the seen prices.
  #
  # Args:
  #   data: A data frame as an output from the 'CountPriceFitScores' function.
  # Returns:
  #   A data frame with total scores and mean scores of Likert scores, and
  #   prices included.

  data %>% dplyr::rowwise() %>%
    dplyr::mutate(totScore = one + 2 * two + 3 * three + 4 * four + 5 * five,
           meanScore = round(totScore / sum(one, two, three, four, five), 2),
           price = as.numeric(str_extract(product, "[0-9].[0-9][0-9]")))
}
