ShelfFreqPlot <- function(data = data.df, group_colour = "#abd9e9") {
  # Plot the frequencies of choices as bar graph.
  #
  # Args:
  #   data: A data frame as an output from the SelectChoice function.
  #   group_colour: Colour that will be used for the bars.
  # Returns:
  #   A bar graph describing the frequencies of choices from the shelf.

  print(ggplot(data = data) +
          geom_bar(aes(x = x_var, y = y_var), stat = "identity",
                   fill = group_colour) +
          geom_text(aes(x = x_var, y = y_var - 2, label = y_var), size = 8,
                    family = "Futura Light", colour = "#413e36") +
          theme(panel.grid.major = element_blank(),
                panel.grid.minor = element_blank(),
                panel.border = element_blank(),
                panel.background = element_blank(),
                plot.background = element_rect(fill = "transparent",
                                               colour = NA),
                axis.title.y = element_blank(), axis.text.y = element_blank(),
                axis.title.x = element_blank(),
                text = element_text(family = "Futura Light",
                                    colour = "#413e36"),
                axis.ticks = element_blank(),
                plot.margin = unit(c(0, -7, 2, -7), "mm")))
}
