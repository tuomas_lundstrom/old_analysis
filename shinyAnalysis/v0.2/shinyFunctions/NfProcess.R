#-----------------------------------------------------#
#       Evaluating the process map for the data
#-----------------------------------------------------# 
NfProcess <- function(data = procesData()) {
  
  #' Take the JSON file as input and evaluate is. Basicaly a very slightly modified version of Tuomas
  #' @param data A JSON file with all functions and arguments. Output of 'getJSON'
  
  # apply for each function
  l <- lapply(fromJSON(data), function(x) {
    paste0(x[1], "(", paste(x[-1], collapse = ", "), ")")
  })
  
  # put a pipe between each function
  l <- paste(l, collapse = " %>% ")
  
  # parse and evaluate the output
  l <- parse(text = l)
  eval(l)
  
}