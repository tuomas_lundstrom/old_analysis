library(shiny)
library(shinythemes)
library(shinyBS)

shinyUI(
  bootstrapPage(theme = shinytheme("journal"),
                
                # Link to the style 
                #tags$link(rel = "stylesheet", type = "text/css", href = "main.css"),
                
                
                tabsetPanel(
                  
                  #-----------------------------------------------------#
                  #        1. Main tab with data input and visual plot
                  #-----------------------------------------------------#  
                  tabPanel('Plot',
                           fluidPage(
                             
                             #---/ Column with the project and analysis selection
                             column(3,
                                    
                                    #--/ Set the project
                                    h4("Project and analysis"),
                                    #uiOutput("user_ui"),
                                    uiOutput("customer_ui"),
                                    uiOutput("campaign_ui"),
                                    uiOutput("project_ui"),
                                    uiOutput("getData_ui"),
                                    uiOutput("analysis_ui")
                                    #--/ Run button
                                    #actionButton("calculate_in", "Calculate", icon = icon("refresh", lib = "font-awesome"))
                                    ),
                             
                             column(9,
                                    fluidRow(
                                      h4("Parameters Independent"),
                                      uiOutput("independent_ui")
                                      ),
                                      br(),
                                    
                                    fluidRow(
                                      h4("Parameters Dependent"),
                                      uiOutput("dependent_ui")
                                    ),
                                    br(),
                                    
                                    fluidRow(
                                      h4("Parameters Graphical"),
                                      uiOutput("graphical_ui")
                                    ),
                                    
                                    fluidRow(
                                       h4("Main graphical output"),
                                       bsAlert("alert"),
                                       plotOutput('plotNF', height = "700px", width = "100%")
                                       #downloadButton("downloadPlot", label = "Download", class = NULL)
                                     )
                                    )
                           )
                           ),
                  
                  
                  #-----------------------------------------------------#
                  #        2. Tab with the processed data
                  #-----------------------------------------------------#  
                  tabPanel('Data',
                           h4("Raw data"),
                           DT::dataTableOutput('dataTable')
                          ),
                  
                  #-----------------------------------------------------#
                  #        3. Tab with process maps
                  #-----------------------------------------------------#  
                  tabPanel('Process Maps',
                           h4('This is the process map that we are using to calculate the data
                             and produce the graphical output.'),
                           # h5("Process map for the data processing"),
                           textOutput("dataJSON")
                           )
                )
  )
)