###########################
### 180074_oetker_pizza ###
###     First round     ###
###      Analysis       ###
###########################



### Init ###
setwd("~/nf-bitbucket")
source("old_analysis/projects/180074_oetker_pizza/first_round/init.R")
if (!dir.exists(output)) dir.create(output)



### Synapse maps ###
errors <- character()
for (item in items[["text"]]) {
  for(key in unique(data.df$stimuli)) {
    #nWord <- nWords[nWords$key == key & nWords$condition == condition,
    #                item]
    nWord <- 4
    item_name <- ifelse(length(unlist(item)) > 1,
                        paste(unlist(item), collapse = "_"), item)
    # create directory
    f <- paste0(output, "synapse_map/")
    l <- paste0(f, item_name, "_", key)
    if(!dir.exists(f)) dir.create(f, recursive = T)
    
    tryCatch({
      x <- data.df %>%
        SubsetIgraphData(group = "all", select_group = "all",
                         x_var = "stimuli", iKey = key,
                         compare_within = FALSE) %>%
        CombineText(y_var = unlist(item), language = "DE") %>%
        SelectIgraphData(x_var = "stimuli") %>%
        WordFreqIgraph() %>%
        IgraphData(iKey = key, seed_word = NULL, n_word = nWord,
                   typeMap = "full")
      
      if(is.list(x)) {
        if(!dir.exists(f)) dir.create(f, recursive = T)
        MakePlots(PlotIgraph(x, typeMap = "full"), path = l,
                  includeSVG = FALSE)
        if(dev.cur() != 1)  dev.off()
      }
    }, error = function(e) {
      errors <- c(errors, l)
    })
  }
}



### Likert ###
x <- do.call("rbind", lapply(items$likert, function(x) {
  data.df %>% select_("stimuli", y_val = x) %>%
    mutate(item = x)
})) %>%
  group_by(stimuli, item, y_val) %>%
  summarise(n = n()) %>%
  mutate(prop = n / sum(n) * 100) %>%
  group_by(stimuli, item) %>%
  mutate(group_n = sum(n)) %>%
  filter(y_val > 3) %>%
  as.data.frame(., stringAsFactors = FALSE)

write.xlsx(x, paste0(output, "results.xlsx"), "likert", row.names = FALSE)

# Plotting things.
colours <- c(`15` = "#5c89b3", `14` = "#7AB6ED",
             `25` = "#980000", `24` = "#cc4125",
             `35` = "#b45f06", `34` = "#f6b26b",
             `45` = "#86A644", `44` = "#DCEDA0",
             `55` = "#cc4125", `54` = "#e6b8af",
             `65` = "#FFDD48", `64` = "#FFEFA4")
combItems <- c("f17", "f21", "f22", "f25")
x <- x %>%
  mutate(fillFactor = factor(paste0(stimuli, y_val), levels = names(colours)))

for (stack in stacks) {
  # Single items.
  for (item in items$likert) {
    y <- x[x$item == item & x$y_val %in% stack, ] %>%
      dplyr::rename(key = stimuli)
    yLims <- ifelse(rep(length(stack) < 2, 2), c(0, 60), c(0, 100))
    # create directory
    f <- paste0(output, "likert/", paste(stack, collapse = "_"), "/")
    l <- paste0(f, item)
    if(!dir.exists(f)) dir.create(f, recursive = T)
    MakePlots(PlotTopBox(y, colour = colours, yLimits = yLims), path = l)
    if(dev.cur() != 1) dev.off()
  }
  
  # Overview plots for item groups.
  for (item in combItems) {
    item <- grep(item, names(data.df), value = TRUE)
    y <- x[x$item %in% item & x$y_val %in% stack, ] %>%
      dplyr::rename(key = stimuli)
    y$item <- substr(y$item, nchar(y$item) - 2, nchar(y$item) - 2)
    yLims <- ifelse(rep(length(stack) < 2, 2), c(0, 60), c(0, 100))
    # create directory
    f <- paste0(output, "likert/", paste(stack, collapse = "_"), "/")
    l <- paste0(f, substr(item[1], 1, nchar(item[1]) - 4))
    if(!dir.exists(f)) dir.create(f, recursive = T)
    MakePlots(PlotTopBoxOverview(y, colours = colours, coordFlip = FALSE,
                                 includeNegative = FALSE,
                                 yLimits = yLims, withNumbers = FALSE),
              path = l)
    if(dev.cur() != 1) dev.off()
  }
}

z <- data.frame(NULL, stringsAsFactors = FALSE)
for (item in items$likert) {
  for (stack in stacks) {
    if (length(stack) == 1) {
      y <- x[x$item == item, ] %>%
        filter(y_val == 5) %>%
        select(x_var = stimuli, y_val = n, n = group_n) %>%
        as.data.frame(., stringsAsFactors = FALSE) %>%
        ChiSquare(nIncluded = TRUE)
    } else {
      y <- x[x$item == item, ] %>%
        group_by(stimuli, group_n) %>%
        summarise(n = sum(n)) %>%
        select(x_var = stimuli, y_val = n, n = group_n) %>%
        as.data.frame(., stringsAsFactors = FALSE) %>%
        ChiSquare(nIncluded = TRUE)
    }
    if (sum(y <= .05, na.rm = TRUE) >= 1) {
      z <- rbind(z, data.frame(item = item,
                               stack = paste(stack, collapse = "+"),
                               stimuli = paste0(which(y <= 0.05,
                                                      arr.ind = TRUE)[, 2],
                                                "vs",
                                                which(y <= 0.05,
                                                      arr.ind = TRUE)[, 1]),
                               pvalue = y[which(y <= 0.05)],
                               stringsAsFactors = FALSE))
    }
  }
}

if (length(z) > 0) {
  write.xlsx(z, paste0(output, "results.xlsx"), "likert_pvalues",
             row.names = FALSE, append = TRUE)
}

x <- data.df %>% select_("stimuli", .dots = items$likert) %>%
  group_by(stimuli) %>%
  summarise_all(funs(sum(. > 4) / n())) %>%
  t(.)
colnames(x) <- paste0("top_", 1:6)
y <- data.df %>% select_("stimuli", .dots = items$likert) %>%
  group_by(stimuli) %>%
  summarise_all(funs(sum(. > 3) / n())) %>%
  t(.)
colnames(y) <- paste0("top2_", 1:6)
z <- data.frame(item = rownames(x)[-1], cbind(x[-1, ], y[-1, ]),
                stringsAsFactors = FALSE)
write.xlsx(z, paste0(output, "likert_results.xlsx"), row.names = FALSE)



### Emotion ###
x <- select(data.df, key = stimuli, y_val = f18_E1) %>%
  group_by(key, y_val) %>%
  summarise(N = n()) %>%
  mutate(prop = N / sum(N)) %>%
  as.data.frame(., stringAsFactors = FALSE)
write.xlsx(x, paste0(output, "results.xlsx"), "emotion", row.names = FALSE,
           append = TRUE)

# P-values.
z <- data.frame(NULL, stringsAsFactors = FALSE)
for (emo in unique(x$y_val)) {
  y <- x %>% group_by(key) %>%
    mutate(n = sum(N)) %>%
    filter_(paste0("y_val == '", emo, "'")) %>%
    select(x_var = key, y_val = N, n) %>%
    as.data.frame(., stringsAsFactors = FALSE) %>%
    ChiSquare(nIncluded = TRUE)
  
  if (sum(y <= .05, na.rm = TRUE) >= 1) {
    z <- rbind(z, data.frame(item = emo,
                             stimuli = paste0(which(y <= 0.05,
                                                    arr.ind = TRUE)[, 2],
                                              "vs",
                                              which(y <= 0.05,
                                                    arr.ind = TRUE)[, 1]),
                             pvalue = y[which(y <= 0.05)],
                             stringsAsFactors = FALSE))
  }
}

if (length(z) > 0) {
  write.xlsx(z, paste0(output, "results.xlsx"), "emotion_pvalues",
             row.names = FALSE, append = TRUE)
}


# Plotting.
colours <- c(boredom = "#5c89b3", fascination = "#bd597f", desire = "#6cae7e")
y <- x %>% dplyr::rename(item = y_val) %>%
  mutate(prop = prop * 100,
         y_val = 1,
         fillFactor = factor(item, levels = names(colours))) %>%
  dplyr::rename(key = "item", item = "key")
# create directory
f <- paste0(output, "emotion/")
l <- paste0(f, "emotion")
if(!dir.exists(f)) dir.create(f, recursive = T)
# save the plot
MakePlots(PlotTopBoxOverview(y, colours = colours, yLimits = c(0, 60),
                             barOrder = sort(unique(y$item),
                                             decreasing = FALSE)),
          path = l, size = c(5.6, 5))
if(dev.cur() != 1) dev.off()


# Emotion strength likert.
colours <- c(boredom5 = "#5c89b3", boredom4 = "#7AB6ED",
             fascination5 = "#bd597f", fascination4 = "#E56D9A",
             desire5 = "#6cae7e", desire4 = "#80CE96")
x <- data.df %>% select(stimuli, y_val = f19_E1strength, item = f18_E1) %>%
  group_by(stimuli, item, y_val) %>%
  summarise(N = n()) %>%
  mutate(prop = N / sum(N),
         fillFactor = factor(paste0(item, y_val), levels = names(colours))) %>%
  as.data.frame(., stringAsFactors = FALSE)

y <- x %>% filter(item != "boredom" & y_val > 3) %>%
  select(stimuli, item, y_val, prop) %>%
  mutate(y_val = rep(c("4+5", "5"), 12)) %>%
  group_by(stimuli, item) %>%
  mutate(prop = ifelse(y_val == "4+5", sum(prop), prop)) %>%
  as.data.frame(., stringAsFactors = FALSE)
write.xlsx(y, paste0(output, "results.xlsx"), "likert_emotion",
           row.names = FALSE, append = TRUE)

for (stack in stacks) {
  y <- x[x$y_val %in% stack, ] %>%
    filter(item != "boredom") %>%
    mutate(prop = prop * 100) %>%
    dplyr::rename(key = item, item = stimuli)
  yLims <- ifelse(rep(length(stack) < 2, 2), c(0, 60), c(0, 100))
  # create directory
  f <- paste0(output, "emotion/")
  l <- paste0(f, "likert_", paste(stack, collapse = "_"))
  if(!dir.exists(f)) dir.create(f, recursive = T)
  MakePlots(PlotTopBoxOverview(y, colours = colours, coordFlip = FALSE,
                               includeNegative = FALSE, yLimits = yLims,
                               textSize = 4),
            path = l)
  if(dev.cur() != 1) dev.off()
}

z <- data.frame(NULL, stringsAsFactors = FALSE)
for (item in unique(x$item)) {
  for (stack in stacks) {
    if (length(stack) == 1) {
      y <- x[x$item == item, ] %>%
        filter(y_val == 5) %>%
        mutate(n = sum(N)) %>%
        select(x_var = stimuli, y_val = N, n) %>%
        as.data.frame(., stringsAsFactors = FALSE) %>%
        ChiSquare(nIncluded = TRUE)
    } else {
      y <- x[x$item == item, ] %>%
        filter(y_val > 3) %>%
        group_by(stimuli) %>%
        summarise(N = sum(N)) %>%
        mutate(n = sum(N)) %>%
        select(x_var = stimuli, y_val = N, n) %>%
        as.data.frame(., stringsAsFactors = FALSE) %>%
        ChiSquare(nIncluded = TRUE)
    }
    if (sum(y <= .05, na.rm = TRUE) >= 1) {
      z <- rbind(z, data.frame(item = item,
                               stack = paste(stack, collapse = "+"),
                               stimuli = paste0(which(y <= 0.05,
                                                      arr.ind = TRUE)[, 2],
                                                "vs",
                                                which(y <= 0.05,
                                                      arr.ind = TRUE)[, 1]),
                               pvalue = y[which(y <= 0.05)],
                               stringsAsFactors = FALSE))
    }
  }
}

if (length(z) > 0) {
  write.xlsx(z, paste0(output, "results.xlsx"), "likert_emotion_pvalues",
             row.names = FALSE, append = TRUE)
}
