#' Plot word cloud
#' @description Plot the frequencies of word occurrences as a word cloud.
#' 
#' @param data: A data frame containing the frequencies of word occurrences as
#' an output from 'WordFreq' function.
#' 
#' @return A word cloud representing the frequencies of word occurrences.
#' 
#' @import wordcloud
#' @export
#' 
PlotWordCloud <- function(data = data.df) {
  wordcloud(data$word, data$freq, scale = c(5, 0.5), random.order = FALSE,
            rot.per = 0.35, use.r.layout = FALSE,
            colors = brewer.pal(8, "Dark2"))
}