#' Calculate word frequencies
#' 
#' @description Calculate the word frequency and see if we can move further.
#' 
#' @param data: A data frame.
#' 
#' @return A data frame with word frequencies.
#'
#' @import tm
#' @export
#' 
WordFreqIgraph <- function(data = data.df) {
  # Create a corpus matrix with words frequencies (use only words that occured
  # more than minimum frequency)
  data.corpus <- VCorpus(DataframeSource(data),
                         readerControl =
                           list(reader =
                                  readTabular(mapping = list(content = "Text",
                                                             id = "Claim"))))
  
  data.dtm <- DocumentTermMatrix(data.corpus,
                                 control =
                                   list(bounds = list(global = c(2, Inf))))
  wordFreq <- as.matrix(data.dtm)
  Claim <- row.names(wordFreq)
  rownames(wordFreq) <- 1:nrow(wordFreq)
  wordFreq <- as.data.frame(wordFreq) %>% mutate(Claim = Claim)
  return(wordFreq)
}