#' Subset igraph data
#'
#' @description Subset the data for plotting a synapse map, according to within
#' or between groups comparison.
#' 
#' @param data A data frame containing columns for text and grouping variable.
#' @param group Column for the grouping variable.
#' @param select_group If we split data by group, then which group to use, e.g.
#' 'Old', 'Young'.
#' @param x_var The categories for x-axis. Can be e.g. column names like 'key
#' visual', 'AgeGroup'. Shall be length 1.
#' @param iKey The unique factor from the 'x_var' column.
#' @param compare_within A logical indicating whether to compare the groups
#' within (TRUE) or between (FALSE).
#' 
#' @return A subsetted data frame.
#' 
#' @import dplyr
#' @export
#' 
SubsetIgraphData <- function(data = data.df, group = "Condition",
                             select_group = "Nivea", x_var = "key_visual",
                             iKey = "Soft_SkinNow_EN", compare_within = TRUE) {
  if (!select_group %in% "all") {
    if (compare_within) {
      l <- paste0(group, " %in% '", select_group, "'")
      data <- data %>% filter_(l)
    } else {
      l <- paste0("(", group, " %in% '", select_group, "' & ", x_var,
                  " %in% '", iKey, "')", " | (!", group, " %in% '",
                  select_group, "' & !", x_var, " %in% '", iKey, "')")
      data <- data %>% filter_(l)
    }
  }
  return(data)
}
