#' Transform matrix to data frame.
#'
#' @description Remove upper triangle in matrix and melt the data.
#'
#' @param data Input data frame.
#' 
#' @return A data frame.
#' 
#' @import dplyr reshape2
#' @export
#' 
MatrixToDF <- function (data = data) {
  data[upper.tri(data, diag = T)] <- NA #replace upper triangle with NA
  data %>%
    reshape2::melt() %>%
    filter(!is.na(value), value > 0)
}